# -*- coding: utf-8 -*-
from odoo import api,fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    user_pac = fields.Char(string="User", compute="fill_data_stamps", store=True)
    pass_pac = fields.Char(string="Password", compute="fill_data_stamps", store=True)

    def _find_mail_template(self, force_confirmation_template=False):
        res = super(SaleOrder, self)._find_mail_template(force_confirmation_template=False)
        if force_confirmation_template or (self.state == 'sale' and not self.env.context.get('proforma', False)):
            res = self.env['ir.model.data'].xmlid_to_res_id('sale.mail_template_sale_confirmation_stamps', raise_if_not_found=False)
        return res

    @api.depends("invoice_ids.invoice_payment_state", "invoice_ids")
    def fill_data_stamps(self):
        for record in self:
            for invoice in record.invoice_ids:
                if invoice.invoice_payment_state == "paid":
                    products = invoice.invoice_line_ids.mapped("product_id.product_tmpl_id").filtered(
                        lambda x: x.type_sale == "stamps")
                    if products:
                        note = ""
                        for p in products:
                            note += "{User: user Pass: pass}, "
                        record.note = note
                        if not record.user_pac and not record.pass_pac:
                            record.user_pac = "31321"
                            record.pass_pac = "lzkxcjhv"
