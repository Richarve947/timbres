# -*- coding: utf-8 -*-
from odoo import fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    type_sale = fields.Selection([('stamps', 'revenue stamps')])
