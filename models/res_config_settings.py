# -*- coding: utf-8 -*-

from odoo import fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    user_stamps = fields.Char()
    pass_stamps = fields.Char()
