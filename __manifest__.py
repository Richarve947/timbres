# -*- coding: utf-8 -*-
{
    'name': "timbres fiscales",

    'summary': """
        sale of tax stamps by e-commerce
    """,

    'description': """
        Add the extendes for sale of tax stamps by e-commerce
    """,

    'author': "Mérida Soft",
    'website': "https://crm.meridasoft.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'sale',
    'version': '13.0',

    # any module necessary for this one to work correctly
    'depends': ['sale_management', 'website_sale'],


    # always loaded
    'data': [
        'data/mail_data.xml',
        'views/product_template_views.xml',
        'views/res_config_settings_views.xml',
        'views/sale_order_views.xml',
        'views/templates.xml',
    ],
    'images': [],
    'demo': [
    ],
}
